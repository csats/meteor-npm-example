# Meteor with NPM package example

Includes an NPM package with a small wrapper. The package exports the variable
`CSV`, making it availble to the app.

# See also

<https://meteorhacks.com/complete-npm-integration-for-meteor.html>
