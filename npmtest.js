if (Meteor.isClient) {
  // counter starts at 0
  Session.setDefault("counter", 0);

  Template.hello.helpers({
    counter: function () {
      return Session.get("counter");
    }
  });

  Template.hello.events({
    'click button': function () {
      // increment the counter when button is clicked
      //Session.set("counter", Session.get("counter") + 1);
      console.log("clicked");
      Meteor.call('do-csv');
    }
  });
}

Meteor.methods({
  'do-csv': function() {
    if (this.isSimulation) {
      console.log('doing csv (client)');
    } else {
      console.log('doing csv (server)');
      var csvGenerate = Meteor.wrapAsync(CSV.generate, CSV);
      var result = csvGenerate({seed: 1, columns: 2, length: 20});
      console.log(result);
    }
  }
});
