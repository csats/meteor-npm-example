Package.describe({
  summary: "Wrap npm csv package",
  version: "1.0.0",
  name: "npm-csv-wrapper",
});

Package.onUse(function (api) {
  api.export('CSV', 'server');
  api.addFiles('exports.js', 'server');
});

Npm.depends({
  'csv': '0.4.0'
});
